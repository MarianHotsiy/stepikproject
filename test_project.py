from selenium import webdriver
import time
import unittest

class TestClass(unittest.TestCase):
    def test1(self):
        link = "http://suninjuly.github.io/registration1.html"
        browser = webdriver.Chrome()
        browser.get(link)

        name = browser.find_element_by_css_selector('.first_block .first_class input').send_keys('name')
        surname = browser.find_element_by_css_selector('.first_block .second_class input').send_keys('dfgh')
        email = browser.find_element_by_css_selector('.first_block .third_class input').send_keys('naівапme')


        # Отправляем заполненную форму
        button = browser.find_element_by_css_selector("button.btn")
        button.click()

        # Проверяем, что смогли зарегистрироваться
        # ждем загрузки страницы
        time.sleep(1)

        # находим элемент, содержащий текст
        welcome_text_elt = browser.find_element_by_tag_name("h1")
        # записываем в переменную welcome_text текст из элемента welcome_text_elt
        welcome_text = welcome_text_elt.text

        # с помощью assert проверяем, что ожидаемый текст совпадает с текстом на странице сайта
        self.assertEqual(welcome_text, "Поздравляем! Вы успешно зарегистировались!" )

    def test2(self):
        link = "http://suninjuly.github.io/registration2.html"
        browser = webdriver.Chrome()
        browser.get(link)

        name = browser.find_element_by_css_selector('.first_block .first_class input').send_keys('name')
        surname = browser.find_element_by_css_selector('.first_block .second_class input').send_keys('dfgh')
        email = browsepr.find_element_by_css_selector('.first_block .third_class input').send_keys('naівапme')


        # Отправляем заполненную форму
        button = browser.find_element_by_css_selector("button.btn")
        button.click()

        # Проверяем, что смогли зарегистрироваться
        # ждем загрузки страницы
        time.sleep(1)

        # находим элемент, содержащий текст
        welcome_text_elt = browser.find_element_by_tag_name("h1")
        # записываем в переменную welcome_text текст из элемента welcome_text_elt
        welcome_text = welcome_text_elt.text

        # с помощью assert проверяем, что ожидаемый текст совпадает с текстом на странице сайта
        self.assertEqual(welcome_text, "Поздравляем! Вы успешно зарегистировались!" )


if __name__ == "__name__":
    unittest.main()